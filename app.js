
// #region Kata 1: Basketball Points
function countBasketballPoints(noOf2PointGoals, noOf3PointGoals) {
  if (noOf2PointGoals < 0 || noOf3PointGoals < 0) {
    console.warn("Invalid input to countBasketballPoints");
    return "Invalid input";
  }
  let total2PointScore = noOf2PointGoals * 2;
  let total3PointScore = noOf3PointGoals * 3;

  let totalScore = total2PointScore + total3PointScore;
//   console.info(totalScore);
  return totalScore;
};

function countPointsAndShowResult(){
    console.log("countPoints hit");
    let twoPointInput = document.getElementById("twoPointInput");
    let threePointInput = document.getElementById("threePointInput");
    let outputElement = document.getElementById("basketballScoreResult");
   
    outputElement.innerText = countBasketballPoints(twoPointInput.value, threePointInput.value);;
};

// #region Quick tests
// countBasketballPoints(1, 1); // 5
// countBasketballPoints(2, -1); // warning message
// countBasketballPoints(7, 5); // 29
// countBasketballPoints(38, 8); // 100
// countBasketballPoints(-1, 5); // warning message
// countBasketballPoints(0, 1); // 3
// countBasketballPoints(0, 0); // 0
// #endregion